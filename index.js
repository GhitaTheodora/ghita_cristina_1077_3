const FIRST_NAME = "Ghita";
const LAST_NAME = "Cristina Theodora";
const GRUPA = "1077C";

/**
 * Make the implementation here
 */
class Employee {
    
    constructor(name,surname,salary)
    {
        this.name=name;
        this.surname=surname;
        this.salary=salary;
    }

   
    getDetails()
    {
        return this.name+' '+this.surname+' '+this.salary;
    }
}

class SoftwareEngineer extends Employee{
   constructor(name,surname,salary,experience){
       super(name,surname,salary);
       if(arguments[3]==undefined)
        this.experience='JUNIOR';
        else this.experience=experience;
   }

   applyBonus(){
       let salary=0;
       if(this.experience=="SENIOR")
        salary=this.salary+this.salary*0.2;
        else if(this.experience=="MIDDLE")
        salary=this.salary+this.salary*0.15;
        else
        salary=this.salary+this.salary*0.1;
        return salary;
   }
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}